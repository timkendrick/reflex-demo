# Reflex demo

This repository contains some examples of basic Reflex applications.

## Installation instructions

These demos assume that you have already [installed Rust](https://www.rust-lang.org/tools/install), and that the `reflex-server` executable has been installed as follows:

```shell
cargo install --git https://gitlab.com/timkendrick/reflex.git \
  reflex-cli \
  reflex-server
```

Note that this will also install the `reflex-cli` tool, which can be used as follows to evaluate scripts:

```shell
reflex-cli --syntax js              # Launches an interactive REPL session
reflex-cli --syntax js ./script.js  # Evaluates the provided JS module, returning the default export
```

See the [`/examples/timestamp.js`](./examples/timestamp.js) example to see the CLI in action:

```shell
reflex-cli --syntax js ./examples/timestamp.js
```

## Demo limitations

- This version is a proof of concept and various features haven't yet been implemented. Notably it isn't at all garbage collected: any subscriptions will not be cleaned up despite the client closing the connection, so expect the server to get bogged down over time.
- The GraphQL server doesn't currently expose a schema, so any introspection queries will fail to load

## Example 1: Relational REST API

### Overview

This demo illustrates how to build a simple relational REST API. It ties together multiple dummy REST endpoints from the [JSON Placeholder](https://jsonplaceholder.typicode.com/) service into a single nested GraphQL API.

### Usage instructions

Assuming the `reflex-server` executable is on your `PATH`, you can launch the server as follows:

```shell
reflex-server ./examples/rest.js --port 3000
```

You can then access the GraphQL playground at [http://localhost:3000/](http://localhost:3000/).

The server doesn't currently expose a GraphQL schema, so for the time being you'll have to look at [the source code](./examples/rest.js) to figure out the structure. Here's an example query to get you started:

```
query {
  users {
    name
    email
    posts {
      title
      body
    }
    albums {
      title
      photos {
        title
        url
      }
    }
  }
}
```

## Example 2: Streaming GraphQL microservices

### Overview

This demo ties together several interconnected streaming data sources into a single unified GraphQL subscription API.

This relies on several dummy GraphQL microservices which emit fake data, and combines them into a single streaming API.

The data contains foreign key relations that span the different microservices, and the streaming APIs emit at different rates. These are 'live-joined' by the Reflex runtime so that response payloads are automatically recalculated whenever necessary.

### Usage instructions

#### Step 1: launch dummy microservices

- The dummy microservices are written in Node.js and exist in the [`/api`](./api) directory
- Run `npm install` within the [`/api`](./api) directory to install 3rd-party dependencies for the dummy microservices
- Run the following commands in separate terminals from within the [`/api`](./api) directory:

  ```shell
  # Launch a GraphQL server at http://localhost:3001/
  PORT=3001 npm run start:securities

  # Launch a GraphQL server at http://localhost:3002/
  PORT=3002 npm run start:prices

  # Launch a GraphQL server at http://localhost:3003/
  PORT=3003 npm run start:currencies

  # Launch a GraphQL server at http://localhost:3004/
  PORT=3004 npm run start:portfolio
  ```

- Each service exposes a GraphQL playground at the server root (e.g. [http://localhost:3001/](http://localhost:3001/))
- WebSocket subscription servers are exposed at `/graphql` path where applicable (e.g. [ws://localhost:3002/graphql](ws://localhost:3002/graphql))
- The dummy services serve the following API schemas:
  - [`securities.graphql`](./api/src/services/securities.graphql): Static security metadata server
  - [`prices.graphql`](./api/src/services/prices.graphql): Live-updating price server
  - [`currencies.graphql`](./api/src/services/currencies.graphql): Live-updating exchange rates
  - [`porfolio.graphql`](./api/src/services/portfolio.graphql): Static portfolio listing

#### Step 2: launch the server

Assuming the dummy microservices are running, and the `reflex-server` executable is on your `PATH`, you can launch the server as follows:

```
SECURITY_API_URL=http://localhost:3001/ \
PRICES_API_URL=ws://localhost:3002/graphql \
CURRENCIES_API_URL=ws://localhost:3003/graphql \
PORTFOLIO_API_URL=ws://localhost:3004/graphql \
  reflex-server ./examples/graphql.js --port 3000
```

You can then access the GraphQL playground at [http://localhost:3000/](http://localhost:3000/).

The server doesn't currently expose a schema, so for the time being you'll have to look at [the source code](./examples/graphql.js) to figure out the structure. Here are some example queries to get you started:

```
subscription {
  security(securityId: 3) {
    securityId
    securityName
    USD: price(currency: USD)
    EUR: price(currency: EUR)
    GBP: price(currency: GBP)
  }
}
```

```
query {
  portfolio(bookId: 1) {
    positions(offset: 0, limit: 10) {
      security {
        securityId
        securityName
        USD: price(currency: USD)
        EUR: price(currency: EUR)
        GBP: price(currency: GBP)
      }
      exposure {
        long
        short
        total
      }
      axioma: risk(riskModelId: 1) {
        alpha
        beta
        idio
        total
      }
      barra: risk(riskModelId: 2) {
        alpha
        beta
        idio
        total
      }
    }
  }
}
```

> Note that the GraphQL playground UI starts struggling after receiving the first few subscription responses, so you might need to reload the playground window before long

You can adjust the rate of updates by passing custom `UPDATE_INTERVAL` and `UPDATE_PROBABILITY` environment variables when initialising the dummy services - see the [`prices.ts`](./api/src/services/prices.ts) and [`currencies.ts`](./api/src/services/currencies.ts) source code for details.
