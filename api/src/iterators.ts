export function flow<V>(): (value: AsyncIterator<V>) => V;
export function flow<V, T1>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>): (value: AsyncIterator<V>) => AsyncIterator<T1>;
export function flow<V, T1, T2>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>, fn2: (value: AsyncIterator<T1>) => AsyncIterator<T2>): (value: AsyncIterator<V>) => AsyncIterator<T2>;
export function flow<V, T1, T2, T3>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>, fn2: (value: AsyncIterator<T1>) => AsyncIterator<T2>, fn3: (value: AsyncIterator<T2>) => AsyncIterator<T3>): (value: AsyncIterator<V>) => AsyncIterator<T3>;
export function flow<V, T1, T2, T3, T4>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>, fn2: (value: AsyncIterator<T1>) => AsyncIterator<T2>, fn3: (value: AsyncIterator<T2>) => AsyncIterator<T3>, fn4: (value: AsyncIterator<T3>) => AsyncIterator<T4>): (value: AsyncIterator<V>) => AsyncIterator<T4>;
export function flow<V, T1, T2, T3, T4, T5>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>, fn2: (value: AsyncIterator<T1>) => AsyncIterator<T2>, fn3: (value: AsyncIterator<T2>) => AsyncIterator<T3>, fn4: (value: AsyncIterator<T3>) => AsyncIterator<T4>, fn5: (value: AsyncIterator<T4>) => AsyncIterator<T5>): (value: AsyncIterator<V>) => AsyncIterator<T5>;
export function flow<V, T1, T2, T3, T4, T5, T6>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>, fn2: (value: AsyncIterator<T1>) => AsyncIterator<T2>, fn3: (value: AsyncIterator<T2>) => AsyncIterator<T3>, fn4: (value: AsyncIterator<T3>) => AsyncIterator<T4>, fn5: (value: AsyncIterator<T4>) => AsyncIterator<T5>, fn6: (value: AsyncIterator<T5>) => AsyncIterator<T6>): (value: AsyncIterator<V>) => AsyncIterator<T6>;
export function flow<V, T1, T2, T3, T4, T5, T6, T7>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>, fn2: (value: AsyncIterator<T1>) => AsyncIterator<T2>, fn3: (value: AsyncIterator<T2>) => AsyncIterator<T3>, fn4: (value: AsyncIterator<T3>) => AsyncIterator<T4>, fn5: (value: AsyncIterator<T4>) => AsyncIterator<T5>, fn6: (value: AsyncIterator<T5>) => AsyncIterator<T6>, fn7: (value: AsyncIterator<T6>) => AsyncIterator<T7>): (value: AsyncIterator<V>) => AsyncIterator<T7>;
export function flow<V, T1, T2, T3, T4, T5, T6, T7, T8, T9>(fn1: (value: AsyncIterator<V>) => AsyncIterator<T1>, fn2: (value: AsyncIterator<T1>) => AsyncIterator<T2>, fn3: (value: AsyncIterator<T2>) => AsyncIterator<T3>, fn4: (value: AsyncIterator<T3>) => AsyncIterator<T4>, fn5: (value: AsyncIterator<T4>) => AsyncIterator<T5>, fn6: (value: AsyncIterator<T5>) => AsyncIterator<T6>, fn7: (value: AsyncIterator<T6>) => AsyncIterator<T7>, fn8: (value: AsyncIterator<T7>) => AsyncIterator<T8>, fn9: (value: AsyncIterator<T8>) => AsyncIterator<T9>): (value: AsyncIterator<V>) => AsyncIterator<T9>;
export function flow<V, T>(...args: Array<(value: any) => any>): (value: AsyncIterator<V>) => AsyncIterator<T>;
export function flow<V, T>(...args: Array<(value: any) => any>): (value: AsyncIterator<V>) => AsyncIterator<T> {
  return (value: AsyncIterator<V>) => args.reduce((acc, fn) => (value) => fn(acc(value)), (value: AsyncIterator<V>) => value)(value);
}

function createAsyncIterable<T, TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>): AsyncIterator<T, TReturn, TNext> & AsyncIterable<T> {
  return Object.assign(
    {
      next: iterator.next.bind(iterator),
      return: iterator.return && iterator.return.bind(iterator),
      throw: iterator.throw && iterator.throw.bind(iterator),
      [Symbol.asyncIterator]: function() {
        return this;
      },
    });
}

export function subject<T>(): AsyncIterable<T> & {
  next: (value: T) => void,
} {
  const subscriptions = new Array<(value: T) => void>();
  return {
    [Symbol.asyncIterator]: () => {
      const queue = new Array<T>();
      return createAsyncIterable({
        next: () => new Promise(next).then((value) => ({ value, done: false })),
      })

      function next(resolve: (value: T) => void): void {
        if (queue.length > 0) return resolve(queue.shift()!);
        let callback = (value: T) => {
          subscriptions.splice(subscriptions.indexOf(callback), 1);
          queue.push(value);
          next(resolve)
        };
        subscriptions.push(callback);
      }
    },
    next(value) {
      subscriptions.forEach((callback) => callback(value));
    }
  };
}

export function toPromise<T>(iterator: AsyncIterator<T>): Promise<T> {
  return iterator.next().then((payload) => {
    if (iterator.return) iterator.return();
    if (payload.done) {
      throw new Error("No result received");
    } else {
      return payload.value;
    }
  });
}

export function tap<T>(callback: (value: T) => void): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<T, TReturn, TNext> {
  return <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => createAsyncIterable({
    next: (...args: [] | [TNext]) => iterator.next(...args).then((payload): IteratorResult<T> => {
      if (!payload.done) callback(payload.value);
      return payload;
    }),
    return: iterator.return && iterator.return.bind(iterator),
    throw: iterator.throw && iterator.throw.bind(iterator),
  });
}

export function map<T, V>(iteratee: (value: T) => V): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<V, TReturn, TNext> {
  return <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => createAsyncIterable({
    next: (...args: [] | [TNext]) => iterator.next(...args).then((payload): IteratorResult<V> => payload.done ? payload : ({
      done: false,
      value: iteratee(payload.value),
    })),
    return: iterator.return && iterator.return.bind(iterator) as AsyncIterator<V, TReturn, TNext>['return'],
    throw: iterator.throw && iterator.throw.bind(iterator) as AsyncIterator<V, TReturn, TNext>['throw'],
  });
}

export function filter<T, V extends T>(predicate: (value: T) => value is V): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<V, TReturn, TNext>;
export function filter<T>(predicate: (value: T) => boolean): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<T, TReturn, TNext>;
export function filter<T>(predicate: (value: T) => boolean): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<T, TReturn, TNext> {
  return <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => {
    return createAsyncIterable<T, TReturn, TNext>({
      next: (...args: [] | [TNext]) => new Promise((resolve, reject) => next(args, resolve, reject)),
      return: iterator.return && iterator.return.bind(iterator),
      throw: iterator.throw && iterator.throw.bind(iterator),
    });

    function next(args: [] | [TNext], resolve: (value: IteratorResult<T>) => void, reject: (error: any) => void) {
      iterator
        .next(...args)
        .then((payload) => {
          if (payload.done === true) {
            resolve(payload);
            return;
          }
          const filterResult = predicate(payload.value);
          if (filterResult) {
            resolve(payload);
          } else {
            next(args, resolve, reject);
          }
        })
        .catch(reject);
    }
  };
}

export function scan<I, T, V>(seed: I, iteratee: (current: I | V, value: T) => V): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<V, TReturn, TNext> {
  return <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => {
    let current: I | V = seed;
    return createAsyncIterable({
      next: (...args: [] | [TNext]) => iterator.next(...args).then((payload): IteratorResult<V> => payload.done ? payload : ({
        done: false,
        value: (current = iteratee(current, payload.value)),
      })),
      return: iterator.return && iterator.return.bind(iterator) as AsyncIterator<V, TReturn, TNext>['return'],
      throw: iterator.throw && iterator.throw.bind(iterator) as AsyncIterator<V, TReturn, TNext>['throw'],
    });
  }
}

export function withInitialValue<T>(initialValue: T): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<T, TReturn, TNext> {
  return (iterator) => {
    let isFirstIteration = true;
    return createAsyncIterable({
      next: (...args) => {
        if (isFirstIteration) {
          isFirstIteration = false;
          return Promise.resolve({ value: initialValue, done: false });
        }
        return iterator.next(...args);
      },
      return: iterator.return && iterator.return.bind(iterator),
      throw: iterator.throw && iterator.throw.bind(iterator),
    });
  };
}

export function withUnsubscribe<T>(callback: () => void): <TReturn, TNext>(iterator: AsyncIterator<T, TReturn, TNext>) => AsyncIterator<T, TReturn, TNext> {
  return (iterator) => {
    return createAsyncIterable(Object.assign({
      next: iterator.next.bind(iterator),
      return: ((existingReturn) => () => {
        callback();
        return existingReturn ? existingReturn.call(iterator) : Promise.resolve({ value: undefined, done: true });
      })(iterator.return),
      throw: iterator.throw && iterator.throw.bind(iterator),
    }));
  };
};
