import { flow, map, filter, withInitialValue, withUnsubscribe, scan, subject, tap } from './iterators';

interface SubscriptionCacheEntry<T> {
  refCount: number;
  value: T;
}

export class SubscriptionCache<K, T> {
  private factory: (key: K) => T;
  private cache = new Map<K, SubscriptionCacheEntry<T>>();
  private updates = subject<Array<[K, T]>>();

  constructor(factory: (key: K) => T) {
    this.factory = factory;
  }

  public update(getUpdates: (entries: Array<[K, T]>) => Array<[K, T]>): void {
    const updates = getUpdates(Array.from(this.cache).map(([key, { value }]) => [key, value]));
    if (updates.length > 0) this.updates.next(updates);
  }

  public subscribeOne(key: K): AsyncIterator<T> {
    const initialValue = this.retrieveCacheEntry(key);
    const updates = this.updates[Symbol.asyncIterator]();
    return flow(
      map((updates: Array<[K, T]>) => updates.find(([updatedKey]) => updatedKey === key)),
      filter((update): update is NonNullable<typeof update> => update != null),
      map(([, value]) => value),
      withInitialValue(initialValue),
      withUnsubscribe(() => this.disposeCacheEntry(key)),
    )(updates);
  }

  public subscribeMany(keys: Array<K>): AsyncIterator<Array<T>> {
    const initialValues = new Map(keys.map((key) => [key, this.retrieveCacheEntry(key)]));
    const keyset = new Set(keys);
    const updates = this.updates[Symbol.asyncIterator]();
    return flow(
      map((updates: Array<[K, T]>) => updates.filter(([updatedKey]) => keyset.has(updatedKey))),
      filter((updates) => updates.length > 0),
      scan(initialValues, (currentValues: Map<K, T>, updates) => {
        const updatedValues = new Map(currentValues);
        updates.forEach(([key, value]) => updatedValues.set(key, value));
        return updatedValues;
      }),
      withInitialValue(initialValues),
      map((values) => keys.map((key) => values.get(key)!)),
      withUnsubscribe(() => keys.forEach((key) => this.disposeCacheEntry(key))),
    )(updates);
  }

  private retrieveCacheEntry(key: K): T {
    const existingEntry = this.cache.get(key);
    if (existingEntry) {
      existingEntry.refCount++;
      return existingEntry.value;
    } else {
      const entry = { refCount: 1, value: this.factory(key) };
      this.cache.set(key, entry);
      return entry.value;
    }
  }

  private disposeCacheEntry(key: K): void {
    const entry = this.cache.get(key);
    if (!entry) return;
    if (--entry.refCount === 0) {
      this.cache.delete(key);
    }
  }
}
