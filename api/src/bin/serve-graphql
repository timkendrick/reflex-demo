#!/usr/bin/env node
const { ApolloServer } = require('apollo-server');
const fs = require('fs');
const path = require('path');

const args = process.argv.slice(2);

try {
  if (args.length < 1) throw new Error('Missing service definition filename');
  if (args.length > 1) throw new Error(`Multiple service definitions not supported: ${args.join(', ')}`);
  const PORT = process.env.PORT;
  if (!PORT) throw new Error('Missing PORT environment variable');
  const port = Number(PORT);
  if (!Number.isInteger(port) || port <= 0) throw new Error(`Invalid port number: ${PORT}`);
  const [service] = args;
  log(`Loading service definition from ${service}`);
  const servicePath = path.resolve(service);
  if (!fs.existsSync(servicePath)) throw new Error(`Service definition not found: ${servicePath}`);
  let serviceDefinition;
  try {
    serviceDefinition = require(servicePath)
  } catch (error) {
    throw new Error(`Failed to load service definition ${servicePath}: ${error.message}`);
  }
  if (!serviceDefinition.schema) throw new Error('Missing service definition schema');
  if (!serviceDefinition.default) throw new Error('Missing service definition resolver');
  const { schema, default: resolvers } = serviceDefinition;
  if (typeof schema !== 'object') throw new Error('Invalid service definition schema');
  if (typeof resolvers !== 'object') throw new Error('Invalid service definition resolver');
  log(`Launching HTTP server on port ${port}...`);
  new ApolloServer({ typeDefs: schema, resolvers }).listen(port).then(({ url, subscriptionsUrl }) => {
    log(`HTTP server listening on ${url}`);
    log(`WebSocket server listening on ${subscriptionsUrl}`);
  });
} catch (error) {
  process.stderr.write(`${error.message}\n`);
  process.exit(1);
}

function log(message) {
  process.stdout.write(`${message}\n`);
}
