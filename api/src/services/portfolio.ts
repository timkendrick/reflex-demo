import { gql, IResolvers } from 'apollo-server';

export interface PositionExposure {
  securityId: number;
  long: number;
  short: number;
  total: number;
}

export interface PositionRisk {
  securityId: number;
  alpha: number;
  beta: number;
  idio: number;
  total: number;
}

export const schema = gql`
type Query {
  bookName(bookId: Int!): String!
  portfolioExposure(bookId: Int!): [PositionExposure!]!
  portfolioRisk(bookId: Int!, riskModelId: Int!): [PositionRisk!]!
}

type PositionExposure {
  securityId: Int!
  long: Float!
  short: Float!
  total: Float!
}

type PositionRisk {
  securityId: Int!
  alpha: Float!
  beta: Float!
  idio: Float!
  total: Float!
}
`;

const BOOKS = ((books) => new Map(books.map((book) => [book.bookId, book])))([
  createBook({
    bookId: 1,
    bookName: 'USA',
    numSecurities: 250,
  }),
  createBook({
    bookId: 2,
    bookName: 'Europe',
    numSecurities: 1500,
  }),
  createBook({
    bookId: 3,
    bookName: 'All positions',
    numSecurities: 10000,
  }),
]);

const resolvers: IResolvers = {
  Query: {
    bookName: (_, args) => {
      const { bookId } = args;
      if (typeof bookId !== 'number') throw new Error(`Invalid book ID: ${bookId}`);
      const book = BOOKS.get(bookId);
      if (!book) throw new Error(`Book not found: ${bookId}`);
      return book.bookName;
    },
    portfolioExposure: (_, args) => {
      const { bookId } = args;
      if (typeof bookId !== 'number') throw new Error(`Invalid book ID: ${bookId}`);
      const book = BOOKS.get(bookId);
      if (!book) throw new Error(`Book not found: ${bookId}`);
      return book.securityIds.map((securityId) => createPositionExposure({ bookId, securityId }));
    },
    portfolioRisk: (_, args) => {
      const { bookId, riskModelId } = args;
      if (typeof bookId !== 'number') throw new Error(`Invalid book ID: ${bookId}`);
      if (typeof riskModelId !== 'number') throw new Error(`Invalid risk model ID: ${riskModelId}`);
      const book = BOOKS.get(bookId);
      if (!book) throw new Error(`Book not found: ${bookId}`);
      return book.securityIds.map((securityId) => createPositionRisk({ bookId, riskModelId, securityId }));
    },
  },
};

export default resolvers;

function createBook(options: {
  bookId: number,
  bookName: string,
  numSecurities: number,
}): {
  bookId: number,
  bookName: string,
  securityIds: Array<number>,
} {
  const { bookId, bookName, numSecurities } = options;
  return {
    bookId,
    bookName,
    securityIds: Array.from({ length: numSecurities }, (_, index) => (bookId * 10000) + index),
  };
}

function createPositionRisk(options: {
  bookId: number,
  riskModelId: number,
  securityId: number,
}): PositionRisk {
  const { securityId } = options;
  return {
    securityId,
    alpha: Math.random(),
    beta: Math.random(),
    idio: Math.random(),
    total: Math.random(),
  };
}

function createPositionExposure(options: {
  bookId: number,
  securityId: number,
}): PositionExposure {
  const { securityId } = options;
  const exposure = Math.round((-0.5 + Math.random()) * 10000000);
  return {
    securityId,
    long: exposure > 0 ? exposure : 0,
    short: exposure < 0 ? exposure : 0,
    total: exposure,
  };
}
