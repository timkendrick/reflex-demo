import { gql, IResolvers } from 'apollo-server';
import { toPromise } from '../iterators';
import { SubscriptionCache } from '../subscriptions';

const UPDATE_INTERVAL = parseEnvNumber(process => process.env.UPDATE_INTERVAL, 1000);
const UPDATE_PROBABILITY = parseEnvNumber(process => process.env.UPDATE_PROBABILITY, 0.1);
const UPDATE_AMOUNT = parseEnvNumber(process => process.env.UPDATE_AMOUNT, 0.1);

export interface Price {
  securityId: number;
  price: number;
}

export const schema = gql`
type Query {
  securityPrice(securityId: Int!): Price!
  securityPrices(securityId: [Int!]!): [Price!]!
}

type Subscription {
  securityPrice(securityId: Int!): Price!
  securityPrices(securityId: [Int!]!): [Price!]!
}

type Price {
  securityId: Int!
  price: Float!
}
`;

const cache = new SubscriptionCache(createPrice);

setInterval(() => {
  cache.update((values) => values.filter(() => Math.random() < UPDATE_PROBABILITY).map(([key, { securityId, price }]) => [key, {
    securityId,
    price: getUpdatedValue(UPDATE_AMOUNT, price),
  }]));
}, UPDATE_INTERVAL);

const resolvers: IResolvers = {
  Query: {
    securityPrice: (_, args) => toPromise(resolveSecurityPrice(args)),
    securityPrices: (_, args) => toPromise(resolveSecurityPrices(args)),
  },
  Subscription: {
    securityPrice: {
      subscribe: (_, args) => resolveSecurityPrice(args),
      resolve: (payload) => payload,
    },
    securityPrices: {
      subscribe: (_, args) => resolveSecurityPrices(args),
      resolve: (payload) => payload,
    },
  }
};

export default resolvers;

function resolveSecurityPrice(args: Record<string, unknown>): AsyncIterator<Price> {
  const { securityId } = args;
  if (typeof securityId !== 'number') throw new Error(`Invalid security ID: ${securityId}`);
  return cache.subscribeOne(securityId);
}

function resolveSecurityPrices(args: Record<string, unknown>): AsyncIterator<Array<Price>> {
  const { securityId } = args;
  if (!isArrayOfNumbers(securityId)) throw new Error(`Invalid security IDs: ${securityId}`);
  return cache.subscribeMany(securityId);
}

function createPrice(securityId: Price['securityId']): Price {
  return {
    securityId: securityId,
    price: securityId + 0.01,
  };
}

function getUpdatedValue(updateAmount: number, value: number): number {
  return Number((value * (1 + updateAmount * (-0.5 + Math.random()))).toFixed(3))
}

function isArrayOfNumbers(value: unknown): value is Array<number> {
  return Array.isArray(value) && value.every((item) => typeof item === 'number');
}

function parseEnvNumber(factory: (process: NodeJS.Process) => string | undefined, fallback: number): number {
  const value = Number(factory(process));
  return isNaN(value) ? fallback : value;
}
