import { gql, IResolvers } from 'apollo-server';

export interface Security {
  securityId: number;
  securityName: string;
}

export const schema = gql`
type Query {
  securityById(securityId: Int!): Security!
  securitiesById(securityId: [Int!]!): [Security!]!
}

type Security {
  securityId: Int!
  securityName: String!
}
`;

const resolvers: IResolvers = {
  Query: {
    securityById: (_, args) => {
      const { securityId } = args;
      if (typeof securityId !== 'number') throw new Error(`Invalid security ID: ${securityId}`);
      return createSecurity(securityId);
    },
    securitiesById: (_, args) => {
      const { securityId } = args;
      if (!isArrayOfNumbers(securityId)) throw new Error(`Invalid security IDs: ${securityId}`);
      return securityId.map(createSecurity);
    },
  }
};

export default resolvers;

function createSecurity(securityId: Security['securityId']): Security {
  return {
    securityId: securityId,
    securityName: `Security #${securityId}`,
  };
}

function isArrayOfNumbers(value: unknown): value is Array<number> {
  return Array.isArray(value) && value.every((item) => typeof item === 'number');
}
