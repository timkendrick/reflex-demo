import { gql, IResolvers } from 'apollo-server';
import { toPromise } from '../iterators';
import { SubscriptionCache } from '../subscriptions';

import EXCHANGE_RATES from './rates.json';

const UPDATE_INTERVAL = parseEnvNumber(process => process.env.UPDATE_INTERVAL, 1000);
const UPDATE_PROBABILITY = parseEnvNumber(process => process.env.UPDATE_PROBABILITY, 1);
const UPDATE_AMOUNT = parseEnvNumber(process => process.env.UPDATE_AMOUNT, 0.01);

export interface Currency {
  id: string;
  price: number;
}

export const schema = gql`
type Query {
  currency(id: ID!): Currency!
  currencies(id: [ID!]): [Currency!]!
}

type Subscription {
  currency(id: ID!): Currency!
  currencies(id: [ID!]): [Currency!]!
}

type Currency {
  id: ID!
  price: Float!
}
`;

const cache = new SubscriptionCache(createCurrency);

setInterval(() => {
  cache.update((values) => values.filter(() => Math.random() < UPDATE_PROBABILITY).map(([key, { id, price }]) => [key, {
    id,
    price: price === 1 ? price : getUpdatedValue(UPDATE_AMOUNT, price),
  }]));
}, UPDATE_INTERVAL);

const resolvers: IResolvers = {
  Query: {
    currency: (_, args) => toPromise(resolveCurrencyPrice(args)),
    currencies: (_, args) => toPromise(resolveCurrencyPrices(args)),
  },
  Subscription: {
    currency: {
      subscribe: (_, args) => resolveCurrencyPrice(args),
      resolve: (payload) => payload,
    },
    currencies: {
      subscribe: (_, args) => resolveCurrencyPrices(args),
      resolve: (payload) => payload,
    },
  }
};

export default resolvers;

function resolveCurrencyPrice(args: Record<string, unknown>): AsyncIterator<Currency> {
  const { id } = args;
  if (typeof id !== 'string') throw new Error(`Invalid security ID: ${id}`);
  return cache.subscribeOne(id);
}

function resolveCurrencyPrices(args: Record<string, unknown>): AsyncIterator<Array<Currency>> {
  const { id } = args;
  if (!isOptionalArrayOfStrings(id)) throw new Error(`Invalid security IDs: ${id}`);
  return cache.subscribeMany(id || Object.keys(EXCHANGE_RATES));
}

function createCurrency(id: Currency['id']): Currency {
  return {
    id: id,
    price: EXCHANGE_RATES[id as keyof typeof EXCHANGE_RATES] || 1,
  };
}

function getUpdatedValue(updateAmount: number, value: number): number {
  return Number((value * (1 + updateAmount * (-0.5 + Math.random()))).toFixed(3))
}

function isOptionalArrayOfStrings(value: unknown): value is Array<string> | null | undefined {
  if (value == undefined) return true;
  return Array.isArray(value) && value.every((item) => typeof item === 'string');
}

function parseEnvNumber(factory: (process: NodeJS.Process) => string | undefined, fallback: number): number {
  const value = Number(factory(process));
  return isNaN(value) ? fallback : value;
}
