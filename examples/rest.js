import { Resolver } from 'reflex::graphql';
import { fetch } from 'reflex::http';

export default new Resolver({
  query: {
    users: fetch('https://jsonplaceholder.typicode.com/users').json().map(
      ({ id, name, email }) => ({
        id,
        name,
        email,
        posts: fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`).json(),
        albums: fetch(`https://jsonplaceholder.typicode.com/albums?userId=${id}`).json().map(
          ({ id, title }) => ({
            id,
            title,
            photos: fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`).json(),
          })
        ),
      }),
    ),
  },
  mutation: null,
  subscription: null,
});
