import { Resolver } from 'reflex::graphql';
import DataLoader from 'reflex::loader';

import CurrenciesApi from '../api/src/services/currencies.graphql';
import SecurityApi from '../api/src/services/securities.graphql';
import PricesApi from '../api/src/services/prices.graphql';
import PortfolioApi from '../api/src/services/portfolio.graphql';

const currenciesApi = new CurrenciesApi({ url: process.env.CURRENCIES_API_URL });
const securityApi = new SecurityApi({ url: process.env.SECURITY_API_URL });
const pricesApi = new PricesApi({ url: process.env.PRICES_API_URL });
const portfolioApi = new PortfolioApi({ url: process.env.PORTFOLIO_API_URL });

const fetchExchangeRates = () => {
  const { currencies } = currenciesApi.execute({
    query: gql`
      subscription {
        currencies {
          id
          price
        }
      }
    `,
    variables: {},
  });
  return new Map(currencies.map(({ id, price }) => [id, price]));
};

const getExchangeRate = (currency) => fetchExchangeRates().get(currency);

const priceLoader = new DataLoader((securityIds) => {
  const { securityPrices } = pricesApi.execute({
    query: gql`
      subscription ($securityIds: [Int!]!) {
        securityPrices(securityId: $securityIds) {
          securityId
          price
        }
      }
    `,
    variables: {
      securityIds,
    }
  });
  return securityPrices;
});

const securityLoader = new DataLoader((securityIds) => {
  const { securitiesById } = securityApi.execute({
    query: gql`
      query ($securityIds: [Int!]!) {
        securitiesById(securityId: $securityIds) {
          securityId
          securityName
        }
      }
    `,
    variables: {
      securityIds,
    }
  });
  return securitiesById.map(({ securityId, securityName }) => ({
    securityId,
    securityName,
    price: ({ currency }) => priceLoader.load(securityId).price * getExchangeRate(currency),
  }));
});

const fetchBookName = (bookId) => {
  const { bookName } = portfolioApi.execute({
    query: gql`
      query ($bookId: Int!) {
        bookName(bookId: $bookId)
      }
    `,
    variables: {
      bookId,
    }
  });
  return bookName;
};

const fetchPortfolioExposures = (bookId) => {
  const { portfolioExposure } = portfolioApi.execute({
    query: gql`
      query ($bookId: Int!) {
        portfolioExposure(bookId: $bookId) {
          securityId
          long
          short
          total
        }
      }
    `,
    variables: {
      bookId,
    }
  });
  return new Map(portfolioExposure.map(({ securityId, long, short, total }) => [securityId, {
    long,
    short,
    total,
  }]));
};

const fetchPortfolioRisk = (bookId, riskModelId) => {
  const { portfolioRisk } = portfolioApi.execute({
    query: gql`
      query ($bookId: Int!, $riskModelId: Int!) {
        portfolioRisk(bookId: $bookId, riskModelId: $riskModelId) {
          securityId
          alpha
          beta
          idio
          total
        }
      }
    `,
    variables: {
      bookId,
      riskModelId,
    }
  });
  return new Map(portfolioRisk.map(({ securityId, alpha, beta, idio, total }) => [securityId, {
    alpha,
    beta,
    idio,
    total,
  }]));
};

const Query = {
  security: ({ securityId }) => securityLoader.load(securityId),
  exchangeRate: ({ currency }) => getExchangeRate(currency),
  portfolio: ({ bookId }) => {
    const bookName = fetchBookName(bookId);
    const exposures = fetchPortfolioExposures(bookId);
    return {
      name: bookName,
      positions: ({ offset, limit }) => exposures.entries().slice(offset, limit)
        .map(([securityId, exposure]) => ({
          security: securityLoader.load(securityId),
          exposure,
          risk: ({ riskModelId }) => fetchPortfolioRisk(bookId, riskModelId).get(securityId),
        })),
    };
  }
};

export default new Resolver({
  query: Query,
  subscription: Query,
  mutation: null,
});
